/**
 * @NApiVersion 2.x
 * @NScriptType ClientScript
 * @NModuleScope SameAccount
 * @name: 
 * @author: Joaquim Sousa <joaquim.souza@activecs.com.br>
 */

define([], function () {
    function fieldChanged(context) {
        var currentRecord = context.currentRecord;
        var sublistName = context.sublistId;
        var sublistFieldName = context.fieldId;
        if (sublistName === 'expense' && sublistFieldName == 'custcol_acs_categoria_despesa_lista') {

            try {

                var acs_categoria = currentRecord.getCurrentSublistValue({
                    sublistId: sublistName,
                    fieldId: 'custcol_acs_categoria_despesa_lista'
                })

                currentRecord.setCurrentSublistValue({
                    sublistId: sublistName,
                    fieldId: 'category',
                    value: acs_categoria
                });

            } catch (error) {

                alert(error.message)

            }

        } else {

            return true

        }
    }
    return {
        fieldChanged: fieldChanged
    }
})